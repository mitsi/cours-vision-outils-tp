import matplotlib.pyplot as plt
import numpy as np
import cv2
from main_hough import redraw_lines

# create a 2d array to hold the gamestate
gamestate = [["-", "-", "-"], ["-", "-", "-"], ["-", "-", "-"]]

# kernel used for noise removal
kernel = np.ones((7, 7), np.uint8)
# Load a color image
# img = redraw_lines(cv2.imread('./data/board_1.png'))
img = redraw_lines(cv2.imread('./data/tic-tac-toe-150614_960_720.png'))
plt.imshow(img)
plt.show()

img = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
# get the image width and height
img_width = img.shape[0]
img_height = img.shape[1]

# turn into grayscale
img_g = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh1 = cv2.threshold(img_g, 127, 255, cv2.THRESH_BINARY)
# remove noise from binary
morpho = cv2.morphologyEx(thresh1, cv2.MORPH_OPEN, kernel)

# find and draw contours. RETR_EXTERNAL retrieves only the extreme outer contours
contours, hierarchy = cv2.findContours(morpho, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contours, -1, (0, 255, 0), 15)

tileCount = 9
for cnt in contours:
    # ignore small contours that are not tiles
    if cv2.contourArea(cnt) > 100000:
        tileCount = tileCount - 1
        # use boundingrect to get coordinates of tile
        x, y, w, h = cv2.boundingRect(cnt)
        # create new image from binary, for further analysis. Trim off the edge that has a line
        tile = morpho[x + 60:x + w - 50, y + 60:y + h - 50]
        # create new image from main image, so we can draw the contours easily
        imgTile = img[x + 60:x + w - 50, y + 60:y + h - 50]

        # determine the array indexes of the tile
        tileX = round((x / img_width) * 3)
        tileY = round((y / img_height) * 3)

        # find contours in the tile image. RETR_TREE retrieves all of the contours and reconstructs a full hierarchy of nested contours.
        c, hierarchy = cv2.findContours(tile, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for ct in c:
            # to prevent the tile finding itself as contour
            if cv2.contourArea(ct) < 30000:
                cv2.drawContours(imgTile, [ct], -1, (255, 0, 0), 15)
                # calculate the solitity
                area = cv2.contourArea(ct)
                hull = cv2.convexHull(ct)
                hull_area = cv2.contourArea(hull)
                solidity = float(area) / hull_area

                # fill the gamestate with the right sign
                if (solidity > 0.6):
                    gamestate[tileX][tileY] = "O"
                else:
                    gamestate[tileX][tileY] = "X"
        # put a number in the tile
        cv2.putText(img, str(tileCount), (x + 150, y + 300), cv2.FONT_HERSHEY_SIMPLEX, 10, (0, 0, 255), 20)

# print the gamestate
print("Gamestate:")
for line in gamestate:
    linetxt = ""
    for cel in line:
        linetxt = linetxt + "|" + cel
    print(linetxt)

img = cv2.resize(img, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
plt.imshow(img)
plt.show()
