import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
from PIL import Image
import os


class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        out = self.relu(x)
        out = self.fc1(out)
        out = self.relu(out)
        out = self.fc2(out)
        return out


class CellReconizer:

    def __init__(self, epoch):
        self.model_path = "./data/model.torch"
        self.input_size = 100 * 100
        self.hidden_size = 500
        self.num_classes = 3
        self.num_epochs = epoch
        self.learning_rate = 0.001
        self.training_data_transform = transforms.Compose([
            torchvision.transforms.Grayscale(),
            transforms.Resize(size=(100, 100)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
        ])
        self.detection_data_transform = transforms.Compose([
            torchvision.transforms.Grayscale(),
            transforms.Resize(size=(100, 100)),
            transforms.ToTensor(),
        ])
        self.imagenet_data = torchvision.datasets.ImageFolder('./dataset', transform=self.training_data_transform)
        self.data_loader = torch.utils.data.DataLoader(self.imagenet_data, batch_size=5, shuffle=True, num_workers=8)

        if os.path.isfile(self.model_path):
            self.model = self.load()
        else:
            self.model = NeuralNet(self.input_size, self.hidden_size, self.num_classes)
            self.criterion = nn.CrossEntropyLoss()
            self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)
            self.train()

    def get_case_symbol(self, img):
        img = self.detection_data_transform(Image.fromarray(img))
        # plt.imshow(transforms.ToPILImage()(img), cmap="gray")
        # plt.show()
        img = img.reshape(-1, 100 * 100)
        output = self.model(img)
        _, predicted = torch.max(output.data, 1)
        return self.data_loader.dataset.classes[predicted.numpy()[0]]

    def print_test_prediction(self, model, data_loader):
        images, labels = next(iter(data_loader))
        images = images[0]
        plt.imshow(transforms.ToPILImage()(images), cmap="gray")
        plt.show()
        img = images.reshape(-1, 100 * 100)
        output = model(img)
        print(output)
        _, predicted = torch.max(output.data, 1)

        print("--------")
        print("Prediction for image : %s" % data_loader.dataset.classes[predicted.numpy()[0]])

    def train(self):
        losses = []
        for epoch in range(self.num_epochs):
            epoch_losses = []
            for i, (images, labels) in enumerate(self.data_loader):
                # Move tensors to the configured device
                if epoch == -1:
                    plt.imshow(transforms.ToPILImage()(images[0]), cmap="gray")
                    plt.show()
                # for image in images:
                #     plt.imshow(transforms.ToPILImage()(image), cmap="gray")
                #     plt.show()
                images = images.reshape(-1, 100 * 100)
                labels = labels

                # Forward pass
                outputs = self.model(images)
                loss = self.criterion(outputs, labels)

                epoch_losses.append(loss.item())
                # Backward and optimize
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            loss = sum(epoch_losses) / len(epoch_losses)
            print('Epoch [{}/{}], Loss: {:.4f}'.format(epoch + 1, self.num_epochs, loss))
            losses.append(loss)

        labels = [i + 1 for i in range(0, len(losses))]
        self.save()
        plt.plot(labels, losses, 'ro', label="data")
        plt.savefig("./datasets/loss.png")

    def save(self):
        torch.save(self.model.state_dict(), self.model_path)
        print("saving model")

    def load(self):
        model = NeuralNet(self.input_size, self.hidden_size, self.num_classes)
        model.load_state_dict(torch.load(self.model_path))
        model.eval()
        return model
