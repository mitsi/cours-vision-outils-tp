import time

import cv2
import matplotlib.pyplot as plt
import numpy as np

from tictactoe_reconizer.CellReconizer import CellReconizer


class BordReconizer:

    def __init__(self, epoch=5, debug=False):
        self.debug = debug
        self.cell_reconizer = CellReconizer(epoch)

    def get_game_state(self, img):
        if img.shape[0] != 600 and img.shape[1] != 600:
            raise Exception("IMAGE SIZE != 600*600")
        img_denoized = img.copy()
        img_denoized = cv2.fastNlMeansDenoisingColored(img_denoized)
        gray = cv2.cvtColor(img_denoized, cv2.COLOR_BGR2GRAY)
        kernel_size = 3
        blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 2)
        low_threshold = 50
        high_threshold = 150
        edges = cv2.Canny(blur_gray, low_threshold, high_threshold, apertureSize=5)
        rho = 1  # distance resolution in pixels of the Hough grid
        theta = np.pi / 180  # angular resolution in radians of the Hough grid
        threshold = 80  # minimum number of votes (intersections in Hough grid cell)
        min_line_length = 150  # minimum number of pixels making up a line
        max_line_gap = 50  # maximum gap in pixels between connectable line segments
        line_image = np.copy(img) * 0  # creating a blank to draw lines on
        # Run Hough on edge detected image
        # Output "lines" is an array containing endpoints of detected line segments
        lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)
        lines = self.clean_lines(lines)

        points = self.get_sorted_points(lines)
        if self.debug:
            img_copy = img.copy()
            for line in lines:
                cv2.line(line_image, (line.x1, line.y1), (line.x2, line.y2), (255, 0, 0), 5)

            for p in points:
                cv2.line(line_image, (p[1], p[0]), (p[1] + 1, p[0] + 1), (0, 0, 255), 5)

            for index_line, line in enumerate(line_image):
                for index_pixel, pixel in enumerate(line):
                    if all(i == j for i, j in zip(pixel, [255, 0, 0])) or all(
                            i == j for i, j in zip(pixel, [0, 0, 255])):
                        img_copy[index_line][index_pixel] = pixel
            plt.imshow(img_copy, cmap="gray")
            plt.show()
        if len(points) != 4:
            raise Exception("Cannot parse lines in images")
        images_cells = self.get_images_cells(img_denoized, points)
        game_state = []
        for i in images_cells:
            game_state.append(self.cell_reconizer.get_case_symbol(i))
        return game_state

    def get_images_cells(self, img, points, save_data=False):
        cells = []
        images_cell = []
        cells.append(img[0:points[0][0], 0:points[0][1]])
        cells.append(img[0:points[0][0], points[0][1]:points[1][1]])
        cells.append(img[0:points[0][0], points[1][1]:600])
        cells.append(img[points[0][0]:points[2][0], 0:points[0][1]])
        cells.append(img[points[0][0]:points[2][0], points[0][1]:points[1][1]])
        cells.append(img[points[0][0]:points[2][0], points[1][1]:600])
        cells.append(img[points[2][0]:600, 0:points[0][1]])
        cells.append(img[points[2][0]:600, points[0][1]:points[1][1]])
        cells.append(img[points[2][0]:600, points[1][1]:600])

        for c in cells:
            img_g = cv2.cvtColor(c, cv2.COLOR_BGR2GRAY)
            _, thresh1 = cv2.threshold(img_g, 100, 255, cv2.THRESH_BINARY)
            images_cell.append(cv2.morphologyEx(thresh1, cv2.MORPH_OPEN, np.ones((7, 7), np.uint8)))

        if save_data:
            for index, c in enumerate(images_cell):
                plt.imsave("./datasets/c" + str(index) + "_" + str(time.time()) + ".png", c, cmap='gray')
        return images_cell

    def get_sorted_points(self, lines):
        crossing = []
        i = 0
        while i < len(lines):
            j = 0
            while j < len(lines):
                if i != j:
                    point = lines[i].intersect(lines[j])
                    if point != -1 and point not in crossing:
                        crossing.append(point)
                j += 1
            i += 1
            crossing.sort(key=lambda x: (x[0] * x[1]))
        return crossing

    def clean_lines(self, lines):
        lines_for_computing = []
        for line in lines:
            for x1, y1, x2, y2 in line:
                if y2 - 50 <= y1 <= y2 + 50:
                    x1 = 0
                    x2 = 600
                    lines_for_computing.append(Line(x1, y1, x2, y2))
                elif x2 - 50 <= x1 <= x2 + 50:
                    y1 = 0
                    y2 = 600
                    lines_for_computing.append(Line(x1, y1, x2, y2))
        i = 0
        while i < len(lines_for_computing):
            j = 0
            while j < len(lines_for_computing):
                if i != j and lines_for_computing[i].is_the_same(lines_for_computing[j]):
                    lines_for_computing[i].combine(lines_for_computing[j])
                    lines_for_computing.remove(lines_for_computing[j])
                    i = -1
                    j = len(lines_for_computing)
                j += 1
            i += 1
        return lines_for_computing

class Line:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2

    def is_the_same(self, line):
        x1 = line.x1 - 50 <= self.x1 <= 50 + line.x1
        y1 = line.y1 - 50 <= self.y1 <= 50 + line.y1
        x2 = line.x2 - 50 <= self.x2 <= 50 + line.x2
        y2 = line.y2 - 50 <= self.y2 <= 50 + line.y2
        return x1 and y1 and x2 and y2

    def combine(self, line):
        self.x1 = int((line.x1 + self.x1) / 2)
        self.y1 = int((line.y1 + self.y1) / 2)
        self.x2 = int((line.x2 + self.x2) / 2)
        self.y2 = int((line.y2 + self.y2) / 2)

    def intersect(self, line):
        divider = ((self.x1 - self.x2) * (line.y1 - line.y2) - (self.y1 - self.y2) * (line.x1 - line.x2))
        if divider == 0:
            return -1
        px = ((self.x1 * self.y2 - self.y1 * self.x2) * (line.x1 - line.x2) - (self.x1 - self.x2) * (
                line.x1 * line.y2 - line.y1 * line.x2)) / divider
        py = ((self.x1 * self.y2 - self.y1 * self.x2) * (line.y1 - line.y2) - (self.y1 - self.y2) * (
                line.x1 * line.y2 - line.y1 * line.x2)) / divider
        try:
            if 0 <= int(px) <= 600 and 0 <= int(py) <= 600:
                return [int(py), int(px)]
            return -1
        except:
            return -1
