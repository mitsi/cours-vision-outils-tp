from tictactoe_reconizer.BordReconizer import BordReconizer
import cv2
import numpy as np
import matplotlib.pyplot as plt

bord_reconizer = BordReconizer(epoch=150, debug=False)


def print_board(game_state):
    for index, s in enumerate(game_state):
        if index % 3 == 0:
            print()
            print("|", end="")
        print(s, end='|')


def test_image(path, attend_state, plot=False):
    img = cv2.imread(path)
    img = cv2.resize(img, (600, 600))
    if plot:
        plt.imshow(img)
        plt.show()
    state = bord_reconizer.get_game_state(img)
    print_board(state)
    print(np.alltrue(state == attend_state))


test_image('./data/board_1.png', ["O", "-", "X", "O", "X", "-", "O", "-", "-"])
test_image('./data/board_2.png', ["O", "-", "X", "O", "X", "-", "O", "-", "-"]) # error
test_image('./data/board_3.png', ["O", "-", "X", "O", "X", "-", "O", "-", "-"]) # error
# test_image('./data/TEST.jpg') # too much contours
test_image('./data/test_bi_color.png', ["O", "O", "X", "-", "X", "O", "-", "-", "X"])
test_image('./data/test_bi_color_partial_external.png', ["O", "O", "X", "-", "X", "O", "-", "-", "X"])
test_image('./data/tic-tac-toe-150614_960_720.png', ["O", "-", "X", "X", "X", "O", "O", "-", "-"])
test_image('./data/picture_2019-04-26_17-02-20.jpg', ["-", "-", "-", "-", "X", "-", "-", "-", "-"])
